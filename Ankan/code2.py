from __future__ import division
import numpy as np
import scipy.stats
import scipy.optimize
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
import math
#plt.ion()


# Define some quantities

#L = 10
L=2.0 * np.pi # box legth
DT= [0.1,0.2,0.4,0.5,1] # timestep
NT= [100,50,25,20,10] # total number of timesteps
NTOUT=10 # save output every other NTOUT
NG = [8,16,32,64] # number of gridpoints DECIDE: is this the number of cells or number of edges?
N= 1000 # number of particles
WP=1  
QM=-1 # charge over mass (normalized). For electrons = -1
V0=0.2  # mean initial velocity (one for each species)
T=0.0  # thermal velocity (standard deviation for a Maxwellian distribution)
XP1=0.001 # initial perturbation
# Some more?
Q = WP**2/(QM*N/L)
rho_back = Q*N/L
VT = 0.01
#g = g
dt = DT[3]
NT = NT[0]
dx = [L/NG[0], L/NG[1],L/NG[2],L/NG[3]]
KE, PE, TE, P = [],[],[],[]

#fig,ax1 = plt.subplots(1)
fig, ((ax2,ax3),(ax4,ax5)) = plt.subplots(2,2,sharex = True)


l1 = ["t=0","t="+str(NT)]

# Initialize particles
# position-> uniformly distributed in the box

#time = np.arage(NT)
xp = np.zeros(N)
#vp = np.zeros(N)
vel = np.zeros(N/2)
i = np.arange(N)
xp[:] = (2*i[:]+1)*L/(2*N)

vp = np.random.normal(V0,VT,N)
weights = np.ones_like(vp)/len(vp)
#ax1.hist(vp,bins=100,weights=weights,color='r',alpha=0.5,label=l1[0])

#vp = VT*np.random.rand(N)
#pm = np.arage(N)
#pm = 1-2*np.mod(pm,2)
#vp = vp + pm*V0

#plt.plot(xp,vp)

color = np.zeros(N, dtype= str)
color[np.where(vp<0)] = 'r'
color[np.where(vp>0)] = 'b'

# Add a perturbation in the position (small displacement)
mode=1
xp=xp + XP1 * np.sin(2 * np.pi * xp / L * mode)

#plt.hist(vp)
#plt.show()
# check for periodic boundary conditions
xp = [x%L if x >= L else x+0 for x in xp]
xp = [L -np.abs(x%L) if x <0 else x+0 for x in xp]

   
xy = np.vstack([xp,vp])
z = gaussian_kde(xy)(xy)  

#plt.scatter(xp,vp,c=z, s=100, edgecolor='')
#plt.show()
#print Poisson



u = 0
# Main Loop

for g in NG:
    E = np.zeros(g)
    Ep = np.zeros(N)
    phi = np.zeros(g)
    grid = np.zeros(g)
    j = np.arange(g)
    grid[:] = (2*j[:]+1)*L/(2*g)

    x0 = np.zeros(g*g).reshape(g,g)
    x1 = np.zeros(g*g).reshape(g,g)
    x2 = np.zeros(g*g).reshape(g,g)
    dia = -2*np.ones(g)
    dia1 = np.ones(g-1)
    x0 = np.diag(dia,k=0)
    x1 = np.diag(dia1,k=+1)
    x2 = np.diag(dia1,k=-1)
    Poisson = (x0+x1+x2)

    dia2 = -1*np.ones(g-1)
    x5 = np.diag(dia2,k= 1)
    x6 = x2+x5
    x6[g-1][0] = -1
    x6[0][g-1] = 1
    for t in range(NT):
    
        
    # Mover --> Use leapfrog to update positions
        xp[0] = xp[-1] + dt*(vp[-1] + 0.5*QM*dt*Ep[-1])
        xp[1:-1] = xp[0:-2] + dt*(vp[0:-2] + 0.5*QM*dt*Ep[0:-2])
    
        # check for periodic boundary conditions
        for i in range(N):
            if xp[i] >= L:
                xp[i] = xp[i]%L
            if xp[i] < 0:
                xp[i] = L - (np.abs(xp[i])%L)
        #print xp
    
    # Accumulate density from Particles to Grid	(mind the BC)
    # and calculate charge density
        charge = np.zeros(g)
    
        for k in range(g):
            for i in range(N):
                dist1 = np.abs(xp[i] - grid[k])
            #print dist1
                dist2 = L - dist1
            #print dist2
                if dist1 < dist2:
                    d = dist1
                else:
                    d = dist2
                if d < dx[u] :
                    charge[k] += (1-d/dx[u])/N
              
        charge = charge + rho_back/g

        phi = (dx[u]**2)*np.linalg.solve(Poisson, charge)
        #phi[g-1] = phi[0]
        charge = np.dot(Poisson,phi)
        #print charge
    
        E = np.dot(x6,phi)/(2*dx[u])
    
        Ep = np.zeros(N)
    
        for i in range(N):
            for k in range(g):  
             
                dist1 = np.abs(xp[i] - grid[k])
            
                dist2 = L - dist1
    
                if dist1 < dist2:
                    d = dist1
                else:
                    d = dist2
                if d < dx[u] :
                    Ep[i] = Ep[i] + (1-d/dx[u])*E[k]
    
        #print Ep
    
        vp[0:-2] = vp[0:-2] + 0.5*QM*dt*Ep[0:-2] + (QM * Ep[1:-1] * dt/2)
        vp[-1] = vp[-1] + 0.5*QM*dt*Ep[-1] + (QM * Ep[0] * dt/2)
    
    #dynamical time-step
    #DT = dx/np.max(vp)
    u = u+1
    # Put your diagnostics here: calculate energy and momentum 
    KE.append(np.sum(vp*vp))
    PE.append(np.sum(np.abs(E)*np.abs(E)))
    T = np.sum(vp*vp)+ np.sum(np.abs(E)*np.abs(E))
    TE.append(T)
    P.append(np.sum(vp))
    
ax2.plot(NG,KE,label="KE")
ax2.set_title("KE at time=50units")
ax3.plot(NG,PE,label="PE",color = 'r')
ax3.set_title("PE at time=50units")
ax4.plot(NG,TE,label="TE",color = 'g')
ax4.set_title("TE at time=50units")
ax4.set_xlabel("NG")
ax3.set_xlabel("NG")
ax2.set_xlabel("NG")
ax2.set_ylabel("KE (arb units)")
ax3.set_ylabel("PE (arb units)")
ax4.set_ylabel("TE (arb units)")
ax2.legend(loc="upper right")
ax3.legend(loc="lower right")
ax4.legend(loc="upper right")
fig.subplots_adjust(hspace=0.15)
ax5.plot(NG,P,label = "Momentum")
ax5.legend(loc = "upper right")
ax5.set_xlabel("NG")
ax5.set_ylabel("Momentum in arb. units")
ax5.set_title("Momentum at time=50units")
plt.show()