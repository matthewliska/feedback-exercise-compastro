from __future__ import division
import numpy as np
import scipy.stats
import scipy.optimize
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
import math
#plt.ion()


# Define some quantities

#L = 10
L=2.0 * np.pi # box length
DT= 0.5 # timestep
NT= 250 # total number of timesteps
NTOUT=10 # save output every other NTOUT
NG= 32 # number of gridpoints DECIDE: is this the number of cells or number of edges?
N= 1000 # number of particles
particles = [50,100,150,200,250,300,350,400]
WP=1  
QM=-2 # charge over mass (normalized). For electrons = -1
V0=0.2  # mean initial velocity (one for each species)
T=0.0  # thermal velocity (standard deviation for a Maxwellian distribution)
XP1=0.001 # initial perturbation
# Some more?
#Q = WP**2/(QM*N/L)
rho_back = -1
VT = 0.01
thermal = []
dx = L/NG
KE, PE, TE, P = [],[],[],[]

#fig,ax1 = plt.subplots(1)
#fig, ((ax2,ax3),(ax4,ax5)) = plt.subplots(2,2,sharex = True)


g = NG
dt = DT

# Main Loop
for p in particles:
    N = p
    time = np.arange(NT)
    xp = np.zeros(N)
    #vp = np.zeros(N)
    vel = np.zeros(N/2)
    i = np.arange(N)
    xp[:] = (2*i[:]+1)*L/(2*N)

    l1 = ["t=0","t="+str(NT-1)]
    

    vp = VT*np.random.rand(N)
    pm = np.arange(N)
    pm = 1-2*np.mod(pm,2)
    vp = vp + pm*V0

    E = np.zeros(g)
    Ep = np.zeros(N)
    phi = np.zeros(g)
    grid = np.zeros(g)
    j = np.arange(g)
    grid[:] = (2*j[:]+1)*L/(2*g)

    x0 = np.zeros(g*g).reshape(g,g)
    x1 = np.zeros(g*g).reshape(g,g)
    x2 = np.zeros(g*g).reshape(g,g)
    dia = -2*np.ones(g)
    dia1 = np.ones(g-1)
    x0 = np.diag(dia,k=0)
    x1 = np.diag(dia1,k=+1)
    x2 = np.diag(dia1,k=-1)
    Poisson = (x0+x1+x2)

    dia2 = -1*np.ones(g-1)
    x5 = np.diag(dia2,k= 1)
    x6 = x2+x5
    x6[g-1][0] = -1
    x6[0][g-1] = 1
    for t in range(NT):
    
        
    # Mover --> Use leapfrog to update positions
        xp[0] = xp[-1] + dt*(vp[-1] + 0.5*QM*dt*Ep[-1])
        xp[1:-1] = xp[0:-2] + dt*(vp[0:-2] + 0.5*QM*dt*Ep[0:-2])
    
        # check for periodic boundary conditions
        for i in range(N):
            if xp[i] >= L:
                xp[i] = xp[i]%L
            if xp[i] < 0:
                xp[i] = L - (np.abs(xp[i])%L)
        #print xp
    
    # Accumulate density from Particles to Grid	(mind the BC)
    # and calculate charge density
        charge = np.zeros(g)
    
        for k in range(g):
            for i in range(N):
                dist1 = np.abs(xp[i] - grid[k])
            #print dist1
                dist2 = L - dist1
            #print dist2
                if dist1 < dist2:
                    d = dist1
                else:
                    d = dist2
                if d < dx :
                    charge[k] += (1-d/dx)/N
              
        charge = charge + rho_back/g

        phi = (dx**2)*np.linalg.solve(Poisson, charge)
        #phi[g-1] = phi[0]
        charge = np.dot(Poisson,phi)
        #print charge
    
        E = np.dot(x6,phi)/(2*dx)
    
        Ep = np.zeros(N)
    
        for i in range(N):
            for k in range(g):  
             
                dist1 = np.abs(xp[i] - grid[k])
            
                dist2 = L - dist1
    
                if dist1 < dist2:
                    d = dist1
                else:
                    d = dist2
                if d < dx :
                    Ep[i] = Ep[i] + (1-d/dx)*E[k]
    
        #print Ep
    
        vp[0:-2] = vp[0:-2] + 0.5*QM*dt*Ep[0:-2] + (QM * Ep[1:-1] * dt/2)
        vp[-1] = vp[-1] + 0.5*QM*dt*Ep[-1] + (QM * Ep[0] * dt/2)
    
    #dynamical time-step
    #DT = dx/np.max(vp)
    thermal.append(np.std(vp))
    
plt.plot(particles,thermal)
plt.xlabel("Number of particles")
plt.ylabel("Thermal Velocity (arb units)")
plt.title("Dependence of noise on No.of particles")
plt.show()