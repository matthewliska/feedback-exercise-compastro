from __future__ import division
import numpy as np
import scipy.stats
import scipy.optimize
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
import math
#plt.ion()


# Define some quantities

#L = 10
L=2.0 * np.pi # box length
DT= 0.5 # timestep
NT= 250 # total number of timesteps
NTOUT=10 # save output every other NTOUT
NG= 32 # number of gridpoints DECIDE: is this the number of cells or number of edges?
N= 1000 # number of particles
WP=1  
QM=-2 # charge over mass (normalized). For electrons = -1
V0=0.2  # mean initial velocity (one for each species)
T=0.0  # thermal velocity (standard deviation for a Maxwellian distribution)
XP1=0.001 # initial perturbation
# Some more?
Q = WP**2/(QM*N/L)
rho_back = Q*N/L
VT = 0.01
thermal = []
dx = L/NG
KE, PE, TE, P = [],[],[],[]

#fig,ax1 = plt.subplots(1)
#fig, ((ax2,ax3),(ax4,ax5)) = plt.subplots(2,2,sharex = True)


l1 = ["t=0","t="+str(NT)]

# Initialize particles
# position-> uniformly distributed in the box

time = np.arange(NT)
xp = np.zeros(N)
#vp = np.zeros(N)
vel = np.zeros(N/2)
i = np.arange(N)
xp[:] = (2*i[:]+1)*L/(2*N)

l1 = ["t=0","t="+str(NT-1)]
#vp = np.random.normal(V0,VT,N)
#weights = np.ones_like(vp)/len(vp)
#ax1.hist(vp,bins=100,weights=weights,color='r',alpha=0.5,label=l1[0])

vp = VT*np.random.rand(N)
pm = np.arange(N)
pm = 1-2*np.mod(pm,2)
vp = vp + pm*V0

#plt.plot(xp,vp)
weights = np.ones_like(vp)/len(vp)
plt.hist(vp,bins=100,weights=weights,color='r',alpha=0.5,label=l1[0])

color = np.zeros(N, dtype= str)
color[np.where(vp<0)] = 'r'
color[np.where(vp>0)] = 'b'

# Add a perturbation in the position (small displacement)
mode=1
xp=xp + XP1 * np.sin(2 * np.pi * xp / L * mode)

#plt.hist(vp)
#plt.show()
# check for periodic boundary conditions
xp = [x%L if x >= L else x+0 for x in xp]
xp = [L -np.abs(x%L) if x <0 else x+0 for x in xp]


E = np.zeros(NG)
Ep = np.zeros(N)
phi = np.zeros(NG)
grid = np.zeros(NG)
j = np.arange(NG)
grid[:] = (2*j[:]+1)*L/(2*NG)

x0 = np.zeros(NG*NG).reshape(NG,NG)
x1 = np.zeros(NG*NG).reshape(NG,NG)
x2 = np.zeros(NG*NG).reshape(NG,NG)
dia = -2*np.ones(NG)
dia1 = np.ones(NG-1)
x0 = np.diag(dia,k=0)
x1 = np.diag(dia1,k=+1)
x2 = np.diag(dia1,k=-1)
Poisson = (x0+x1+x2)

dia2 = -1*np.ones(NG-1)
x5 = np.diag(dia2,k= 1)
x6 = x2+x5
x6[NG-1][0] = -1
x6[0][NG-1] = 1
    
xy = np.vstack([xp,vp])
z = gaussian_kde(xy)(xy)  

#plt.scatter(xp,vp,c=z, s=100, edgecolor='')
#plt.show()
#print Poisson


# Main Loop
for t in range(NT):
    
        
    # Mover --> Use leapfrog to update positions
    xp[0] = xp[-1] + DT*(vp[-1] + 0.5*QM*DT*Ep[-1])
    xp[1:-1] = xp[0:-2] + DT*(vp[0:-2] + 0.5*QM*DT*Ep[0:-2])
    
    # check for periodic boundary conditions
    for i in range(N):
        if xp[i] >= L:
            xp[i] = xp[i]%L
        if xp[i] < 0:
            xp[i] = L - (np.abs(xp[i])%L)
    #print xp
    
    # Accumulate density from Particles to Grid	(mind the BC)
    # and calculate charge density
    charge = np.zeros(NG)
    
    for k in range(NG):
        for i in range(N):
            dist1 = np.abs(xp[i] - grid[k])
            #print dist1
            dist2 = L - dist1
            #print dist2
            if dist1 < dist2:
                d = dist1
            else:
                d = dist2
            if d < dx :
                charge[k] += (1-d/dx)/N
              
    '''
    for i in range(N):
        jlow = np.floor(xp[i]/dx)
        #print jlow
        if jlow < NG-1:
            charge[jlow + 1] += (xp[i]- grid[jlow])/(dx*N)
            charge[jlow] = (grid[jlow+1] - xp[i])/(dx*N)
        else:
            charge[0] = (xp[i]- grid[jlow])/(dx*N)
            charge[jlow] = (grid[0] - xp[i])/(dx*N)
    '''
    
    charge = charge + rho_back/NG
    #BC
    #charge[NG-1] +=charge[0]
    #charge[0] = charge[NG-1]
    
    #print charge
    #print np.sum(charge)

    phi = (dx**2)*np.linalg.solve(Poisson, charge)
    #phi[NG-1] = phi[0]
    charge = np.dot(Poisson,phi)
    #print charge
    
    E = np.dot(x6,phi)/(2*dx)
    
    Ep = np.zeros(N)
    
    for i in range(N):
        for k in range(NG):  
            
            dist1 = np.abs(xp[i] - grid[k])
            
            dist2 = L - dist1
    
            if dist1 < dist2:
                d = dist1
            else:
                d = dist2
            if d < dx :
                Ep[i] = Ep[i] + (1-d/dx)*E[k]
    
    vp[0:-2] = vp[0:-2] + 0.5*QM*DT*Ep[0:-2] + (QM * Ep[1:-1] * DT/2)
    vp[-1] = vp[-1] + 0.5*QM*DT*Ep[-1] + (QM * Ep[0] * DT/2)
    
    #dynamical time-step
    #DT = dx/np.max(vp)
    
    # Put your diagnostics here: calculate energy and momentum 
    KE.append(np.sum(vp*vp))
    PE.append(np.sum(np.abs(E)*np.abs(E)))
    T = np.sum(vp*vp)+ np.sum(np.abs(E)*np.abs(E))
    TE.append(T)
    P.append(np.sum(vp))
    # Make plots, movies, etc.
    
    if  t==NT-1 :
        weights = np.ones_like(vp)/len(vp)
        plt.hist(vp,bins=100,weights=weights,color='b',alpha=0.5,label=l1[1])
        plt.xlabel("velocities vp (arb units)")
        plt.ylabel("counts")
        plt.title("histogram of vp 1000 particles")
        #plt.show()
        #plt.plot(j,E)
        #plt.scatter(xp,vp, color = color)
        #plt.hist2d(xp, vp, (80, 80), cmap=cm.bwr,vmin=-1.0, vmax=1.5)
        #plt.scatter(xp,vp,c=z, s=100, edgecolor='')
        #plt.xlabel("xp (arb units)")
        #plt.ylabel("vp (arb units)")
        #plt.title("Phase space: N=1000;NG=32;DT=0.5; time="+str(t),fontsize=12)
        #plt.scatter(xp,vp,c=z, s=100, edgecolor='')
        #cbar = plt.colorbar()
        #cbar.set_label("density")
        #plt.plot(j,charge)
        #plt.plot(j,phi)
        #plt.pause(0.01)
        #plt.clf()
        plt.legend()
        plt.show()

'''
ax1.hist(vp,bins = 100,weights=weights,alpha=0.5,label=l1[1])
ax1.set_xlabel("Velocity of particles (in arbitrary units)")
ax1.set_ylabel("counts")
ax1.set_title("Numerical heating;1000 particles;VT=0.01;DT=0.5;NG=32;L=2*pi",fontsize=12)
ax1.legend()
ax2.plot(time,KE,label="KE")
ax3.plot(time,PE,label="PE",color = 'r')
ax4.plot(time,TE,label="TE",color = 'g')
ax4.set_xlabel("Time in arbitrary units")
ax2.set_ylabel("KE (arb units)")
ax3.set_ylabel("PE (arb units)")
ax4.set_ylabel("TE (arb units)")
ax2.legend(loc="upper left")
ax3.legend(loc="upper left")
ax4.legend(loc="upper left")
fig.subplots_adjust(hspace=0.15)
ax5.plot(time,P,label = "Momentum")
ax5.legend()
ax5.set_xlabel("Time in arb. units")
ax5.set_ylabel("Momentum in arb. units")
ax2.set_title("History of KE; DT=0.5;NG=32")
ax3.set_title("History of PE;DT=0.5;NG=32")
ax4.set_title("History of TE;DT=0.5;NG=32")
ax5.set_title("History of momentum;DT=0.5;NG=32")
'''
plt.show()