# Define some quantities
import numpy as np
import pylab as plt 
from scipy import sparse
from scipy.sparse import linalg

L=2.0 * np.pi # box length
DT=0.5 # timestep
NT=1000 # total number of timesteps
NTOUT=25 # save output every other NTOUT
NG=32 # number of gridpoints DECIDE: is this the number of cells or number of edges?
N=10000 # number of particles
WP=1  
QM=-1 # charge over mass (normalized). For electrons = -1
V0=0.2  # mean initial velocity (one for each species)
VT=0.05  # thermal velocity (standard deviation for a Maxwellian distribution)
XP1=0.001 # initial perturbation
# Some more?
rho = -Q * N/L       #background charge density
xp1 = []
dx = L/NG
Q = WP **2 / (QM *N/L) # charge carried by a particle

# Initialize particles
# position-> uniformly distributed in the box
# velocity -> Normal distribution with std VT and mean V0



xp = np.linspace(0, L- (L/N), N ).T     # initialising particle postions
vp = VT * np.random.randn(N)    # velocity distribution
pm = np.arange(N)   #momentum intialisation
pm = 1 - 2*np.mod(pm+1 , 2)     #two stream instability for two different spcies of particles
vp = vp + pm*V0
print type(xp)
print type(vp)
print (vp*DT+xp)
# Add a perturbation in the position (small displacement)
mode=1
xp=xp + XP1 * np.sin(2 * np.pi * xp / L * mode)
KE = np.arange(N)
EE = np.arange(NG)
p = np.concatenate([np.arange(N), np.arange(N)])
Poisson = sparse.spdiags(([1, -2, 1] * np.ones((1, NG-1), dtype = int).T).T, [-1, 0, 1], NG-1, NG-1 )
Poisson = Poisson.tocsc()

# Main Loop
for i in range(0, NT) :

# Mover --> Use leapfrog to update positions
    xp = (vp *DT ) + xp
    # check for periodic boundary conditions
    xp[np.where(xp<0)] += L
    xp[np.where(xp>=L)] -= L

    # Accumulate density from Particles to Grid	(mind the BC)
    csi  = xp / dx
    # and calculate charge density
    g1 = np.floor(csi - 0.5)
    g = np.concatenate((g1, g1 +1))
    # print type(g)
    # Solve Poisson's equation with the new charge density	
    # (solve first laplacian(phi) = -rho
    # and then E = - grad(phi)
    fraz1 = 1 - np.abs(xp/dx - g1 -0.5)
    fraz = np.concatenate((fraz1, 1- fraz1))
    # Once again: are your boundary conditions satisfied?
    g[np.where(g<0)] += NG
    g[np.where(g>NG-1)] -=NG
    # Calculate Electric field on the position of the particles
    # (interpolate particle --> grid)	
    mat = sparse.csc_matrix((fraz, (p,g)), shape = (N, NG))
    # print len(mat)
    rho_final = Q/dx*mat.toarray().sum(axis=0) + rho
    
    Phi = linalg.spsolve(Poisson, -dx**2* rho_final[0:NG-1])
    Phi = np.concatenate((Phi, [0]))
    Eg = (np.roll(Phi, 1) - np.roll(Phi, -1)) / (2*dx)
    print len(Eg)
    # Update particle velocity with leapfrog
    vp= vp + mat*QM * Eg * DT
    Eg_2 = Eg**2            #particle electric energy
    KE = KE + QM*vp**2       # particle kinetic energy
    EE = np.fabs(Eg_2)         # electric energy
    # TE = KE + EE
    # if i == 0:
    pm = pm + QM*vp         # momentum
    if i%25==0 : 
        # plt.scatter(xp, vp)  
        plt.subplot(221)
        plt.scatter(xp, vp) 
        plt.xlabel('$x_p$', fontsize=22)
        plt.ylabel('$v_p$', fontsize= 22)
        plt.title('Evolution of phase-space')
        plt.subplot(212)
        plt.xlabel('$v_p$', fontsize= 22)
        plt.ylabel('$f(v)$', fontsize= 22)
        plt.title('Evolution of particle velocity with perturbation')
        plt.hist(vp, 200, histtype= 'step', color= 'black', label='at t='+str(i))
        plt.legend()
        # plt.xlim(-0.5, 0.5)
        plt.subplot(222)
        heatmap, xedges, yedges = np.histogram2d(vp, xp, bins= (100, 100))
        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
        plt.xlabel('$x_p$', fontsize= 22)
        plt.ylabel('$v_p$', fontsize=22)
        plt.title('Heatmap for the evolution of the instability')

        plt.imshow(heatmap)
        plt.show()



