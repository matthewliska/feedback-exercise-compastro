import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import gridspec
from matplotlib.colors import LogNorm
import scipy.stats
import time

start = time.time()

# animated True or False
anim = False

# initial setup, timestep is adaptive
n_particles = 1000
grid_size = 2 * np.pi
grid_bins = 64
dx = grid_size / (grid_bins)
dt = 0.01
nt = 1000
dx_inv2 = 1 / (2 * dx)
dt_coeff = 0.5 * grid_size / (grid_bins + 1)

x = np.zeros(n_particles)
v = np.zeros(n_particles)

# setting some particle properties and calculating the ion density to make plasma neutral
q = -1.
plasma_freq = 1.
k = 1.
norm_q = plasma_freq ** 2 / (q * n_particles / grid_size)
i_dens = -norm_q * n_particles / grid_size
m = 1.
q_m = q / m

# velocities in the plasma, v_const is the bulk velocity, v_therm is the thermal velocity
v_const = 0.2
v_therm = 0.0

# initializing grid arrays and matrix to solve poisson equation
e_field = np.zeros(grid_bins + 1)
e_dens = np.zeros(grid_bins + 1)
matrix = np.matrix(np.zeros([grid_bins + 1, grid_bins + 1]))
for i in range(grid_bins + 1):
    matrix[i, i] = -2
    matrix[i - 1, i] = 1
    matrix[i, i - 1] = 1

matrix[0, -1] = 0.
matrix[-1, 0] = 0.

# initializing particles homogeneously on grid
def init_pos(x):
    x = np.linspace(0, grid_size, n_particles, endpoint=False)
    return x

# initializing the bulk velocities and adding thermal noise if wanted
def init_vel(v):
    v[np.arange(0, n_particles - 1, 2)] = v_const
    v[np.arange(1, n_particles, 2)] = - v_const
    if v_therm:
        v += np.random.normal(0, v_therm, len(v))
    return v

# imposing boundary conditions on the particle positions
def bounds(x):
    x[x < 0.0] += grid_size
    x[x >= grid_size] -= grid_size
    return x

# using a leapfrog method and linear interpolation to push the phase space of the particles
def push(x, v, e_field, dt):
    x_grid = x / dx
    left_index = np.floor(x_grid).astype('int')
    right_weight = x_grid - left_index
    left_weight = 1.0 - right_weight
    for i in range(n_particles):
        v[i] += q_m * (left_weight[i] * e_field[left_index[i]]
                     + right_weight[i] * e_field[left_index[i] + 1]) * dt
    x += v * dt
    return x, v

# calculating density on grid using a linear smearing kernel
def dens(x):
    e_dens = np.zeros(grid_bins + 1)
    x_interp = x / dx
    ind = np.floor(x_interp).astype('int')
    r_w = x_interp - ind
    l_w = 1. - r_w
    for i in range(n_particles):
        e_dens[ind[i]] += norm_q * l_w[i] / dx
        e_dens[ind[i] + 1] += norm_q * r_w[i] / dx
    # imposing boundary conditions
    e_dens[0] += e_dens[-1]
    e_dens[-1] = e_dens[0]
    return e_dens + i_dens

# solving for potential using linalg.solve and calculating electric field using finite differencing
def field(e_dens, e_field):
    phi = np.linalg.solve(matrix, -e_dens * dx ** 2)
    e_field[1:-1] = -(phi[2:] - phi[:-2]) * dx_inv2
    e_field[0] = -(phi[1] - phi[-1]) * dx_inv2
    e_field[-1] = -(phi[0] - phi[-2]) * dx_inv2
    return e_field, phi

# allocating analysis arrays
# time = np.linspace(0, nt * dt, nt)
# grid = np.linspace(0, grid_size, grid_bins + 1)
# kin = np.zeros(nt)
# pot = np.zeros(nt)
# tot = np.zeros(nt)
# lin_mom = np.zeros(nt)

# initializing phase space
x = init_pos(x)
x += 1e-3 * np.sin(2 * np.pi * x / grid_size)
x = bounds(x)
v = init_vel(v)

if anim:
    def animate(i):
        global x, v, e_field
        # main loop for integrating through time
        for i in range(1):
            dt = 0.5 * grid_size / (grid_bins + 1) / np.max(v)
            x, v = push(x, v, e_field, dt)
            x = bounds(x)
            e_dens = dens(x)
            e_field, phi = field(e_dens, e_field)
        particles.set_data(x, v)
        return particles,

    # running animation of scatter plot
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111, xlim=(0., grid_size), ylim=(-0.7, 0.7))

    particles, = ax.plot([], [], color='black', marker='o', linestyle='', ms=6)

    def init():
        particles.set_data([], [])
        return particles,

    ani = animation.FuncAnimation(fig, animate, frames=nt,
                                  interval=0.001, blit=True, init_func=init)

    # can choose between live plotting and saving
    plt.show()
    # ani.save('ts_thermal.mp4', fps=100, dpi=100)

else:
    # main loop for integrating through time
    for i in range(nt):
        dt = dt_coeff / np.max(v)
        x, v = push(x, v, e_field, dt)
        x = bounds(x)
        e_dens = dens(x)
        e_field, phi = field(e_dens, e_field)
    #     writing to analysis arrays
    #     kin[i] = np.sum(np.power(v, 2))
    #     pot[i] = np.sum(np.power(e_field, 2))
    #     tot[i] = kin[i] + pot[i]
    #     lin_mom[i] = np.sum(v)

    end = time.time()
    print 'The simulation took:', end - start

    # writing output to file
    # np.savetxt('phase_space_two_stream.txt', np.array([x, v]).transpose())
    # np.savetxt('grid_two_stream.txt', np.array([e_dens, phi, e_field]).transpose())
    # np.savetxt('energy_two_stream.txt', np.array([time, pot, kin, tot, lin_mom]).transpose())

    # simple static scatterplot
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111, xlim=(0., grid_size), ylim=(-0.7, 0.7))

    particles, = ax.plot(x, v, color='black', marker='o', linestyle='', ms=6)
    plt.show()