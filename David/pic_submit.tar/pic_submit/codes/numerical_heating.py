import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.colors import LogNorm
import scipy.stats

# for commented code, see two_stream.py

grid_size = 2 * np.pi
grid_bins = 64
dx = grid_size / (grid_bins)
dt = 0.01
nt = 1000

q = -1.
plasma_freq = 1.
k = 1.
m = 1.

v_const = 0.
v_therm = 0.01

e_field = np.zeros(grid_bins + 1)
e_dens = np.zeros(grid_bins + 1)
matrix = np.matrix(np.zeros([grid_bins + 1, grid_bins + 1]))
for i in range(grid_bins + 1):
    matrix[i, i] = -2
    matrix[i - 1, i] = 1
    matrix[i, i - 1] = 1

matrix[0, -1] = 0.
matrix[-1, 0] = 0.

def init_pos(x, n_particles):
    x = np.linspace(0, grid_size, n_particles, endpoint=False)
    return x

def init_vel(v, n_particles):
    v[np.arange(0, n_particles - 1, 2)] = v_const
    v[np.arange(1, n_particles, 2)] = - v_const
    v += np.random.normal(0, v_therm, len(v))
    return v

def bounds(x):
    x[x < 0.0] += grid_size
    x[x >= grid_size] -= grid_size
    return x

def push(x, v, e_field, dt, n_particles):
    x_grid = x / dx
    left_index = np.floor(x_grid).astype('int')
    right_weight = x_grid - left_index
    left_weight = 1.0 - right_weight
    for i in range(n_particles):
        v[i] += q * (left_weight[i] * e_field[left_index[i]]
                     + right_weight[i] * e_field[left_index[i] + 1]) * dt / m
    x += v * dt
    return x, v

def dens(x, n_particles):
    e_dens = np.zeros(grid_bins + 1)
    x_interp = x / dx
    ind = np.floor(x_interp).astype('int')
    r_w = x_interp - ind
    l_w = 1. - r_w
    for i in range(n_particles):
        e_dens[ind[i]] += norm_q * l_w[i] / dx
        e_dens[ind[i] + 1] += norm_q * r_w[i] / dx
    e_dens[0] += e_dens[-1]
    e_dens[-1] = e_dens[0]
    return e_dens + i_dens

def field(e_dens, e_field):
    phi = np.linalg.solve(matrix, -e_dens * dx ** 2)
    e_field[1:-1] = -(phi[2:] - phi[:-2]) / (2 * dx)
    e_field[0] = -(phi[1] - phi[-1]) / (2 * dx)
    e_field[-1] = -(phi[0] - phi[-2]) / (2 * dx)
    return e_field, phi

time = np.linspace(0, nt * dt, nt)
grid = np.linspace(0, grid_size, grid_bins + 1)
kin = np.zeros(nt)
pot = np.zeros(nt)
tot = np.zeros(nt)
lin_mom = np.zeros(nt)

fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3, figsize=(20, 10))

for n_i in np.arange(100, 3100, 300):
    n_particles = n_i

    x = np.zeros(n_particles)
    v = np.zeros(n_particles)
    norm_q = plasma_freq ** 2 / (q * n_particles / grid_size)
    i_dens = -norm_q * n_particles / grid_size

    x = init_pos(x, n_particles)
    x = bounds(x)
    v = init_vel(v, n_particles)
    for i in range(nt):
        dt = 0.5 * grid_size / (grid_bins + 1) / np.max(v)
        x, v = push(x, v, e_field, dt, n_particles)
        x = bounds(x)
        e_dens = dens(x, n_particles)
        e_field, phi = field(e_dens, e_field)
        kin[i] = np.sum(np.power(v, 2))
        pot[i] = np.sum(np.power(e_field, 2))
        tot[i] = kin[i] + pot[i]
        lin_mom[i] = np.sum(v)
    ax3.scatter(n_particles, np.std(v), c='red', s=100)

ax3.set_xlim(0, n_particles * 1.1)
ax3.set_xlabel('# of particles', fontsize=30)
ax3.set_ylabel('Thermal velocity', fontsize=30)
ax3.tick_params(axis='x', labelsize=20)
ax3.tick_params(axis='y', labelsize=20)
ax1.set_xlim(-0.06, 0.06)
ax1.set_xlabel('Velocity', fontsize=30)
ax1.set_ylabel('# of particles', fontsize=30)
ax1.tick_params(axis='x', labelsize=20)
ax1.tick_params(axis='y', labelsize=20)
ax1.hist(v, bins=np.arange(-0.06, 0.06, 0.005), color='green', alpha=0.5, label=r'$t=t_{final}$')
ax1.hist(init_vel(v, n_particles), bins=np.arange(-0.06, 0.06, 0.005), color='red', alpha=0.5, label=r'$t=0$')
ax2.set_xlabel('Time', fontsize=30)
ax2.set_ylabel('Linear momentum', fontsize=30)
ax2.tick_params(axis='x', labelsize=20)
ax2.tick_params(axis='y', labelsize=20)
ax2.plot(time, lin_mom, c='black')
ax4.set_xlabel('Time', fontsize=30)
ax4.set_ylabel('Kinetic energy', fontsize=30)
ax4.tick_params(axis='x', labelsize=20)
ax4.tick_params(axis='y', labelsize=20)
ax4.plot(time, kin, c='black')
ax5.set_xlabel('Time', fontsize=30)
ax5.set_ylabel('Potential energy', fontsize=30)
ax5.tick_params(axis='x', labelsize=20)
ax5.tick_params(axis='y', labelsize=20)
ax5.plot(time, pot, c='black')
ax6.set_xlabel('Time', fontsize=30)
ax6.set_ylabel('Total energy', fontsize=30)
ax6.plot(time, tot, c='black')
ax6.tick_params(axis='x', labelsize=20)
ax6.tick_params(axis='y', labelsize=20)
ax1.legend()
plt.tight_layout()
plt.show()