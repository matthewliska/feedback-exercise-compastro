import numpy as np
from scipy import stats
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.ticker import FormatStrFormatter
import time


def PIC_simulation(L=2 * np.pi, DT=0.5, NT=500, NTOUT=1, NG=64, N=1000,
                   QM=-1., V0=0.0, T=0.01, XP1=0.001, mode=1, save_last=False,
                   save_all=False, two_beam=False, animate=False, vp=None, **kwargs):
    """
    
    :param L: size of the system
    :param DT: Time step size
    :param NT: Number of time steps
    :param NTOUT: Number of time steps between saving data
    :param NG:  Number of gridpoints
    :param N:   Number of particles
    :param QM: Normalized charge of the particles (-1 for electrons)
    :param V0: Average velocity of the particles
    :param T:  Thermal spread of the system 0 (standard deviation of particle velocity)
    :param XP1: Perturbation of the initial postitions of the particles
    :param mode: Changes the period of the perturbation
    :param save_last: Saves the last data point of the simulation if set to True, increases the then length by 1
    :param save_all: Saves Position, velocity, Electrical field per grid point, electrical field per particle, 
                     charge per grid point, phi per grid point and potential per particle
    :param two_beam: Changes the initial velocities to 0.2 and -0.2 with a spread of T
    :param animate: Makes an animation
    :param vp: if not None specifies the initial velocity of the particles
    :param kwargs: keyword arguments for animate
    :return: (x positions.shape(NT / NTOUT, N), velocities.shape(NT / NTOUT, N)) size increased by one if save_last
             Will also return all other arrays specified by save_all if save_all == True
    """

    # Timer
    start = time.time()
    # Define the poisson matrix
    poisson_matrix = np.zeros((NG + 2, NG + 2))

    for i in range(1, NG + 1):
        poisson_matrix[i][i] = -2
        poisson_matrix[i][i + 1] = 1
        poisson_matrix[i][i - 1] = 1

    poisson_matrix = poisson_matrix[1:NG + 1, 1:NG + 1]
    # Inverse it
    inverse_poisson_matrix = np.linalg.inv(poisson_matrix)

    # Define grid points, from 0 to L, not including L
    grid_positions, grid_size = np.linspace(0, L, NG, endpoint=False, retstep=True)

    # Initialize particles (in two instances for two beam instability)
    xp = np.zeros(N)
    xp[N / 2 + N % 2:] = np.linspace(0, np.pi * 2, N / 2)
    xp[:N / 2] = np.linspace(0, np.pi * 2, N / 2)
    # Perturb positions, keep them in the grid with % L
    xp = (xp + XP1 * np.sin(2 * np.pi * xp / L * mode)) % L

    # Initialize particle velocities
    if two_beam:
        vp = np.zeros(N)
        vp[:N / 2] = stats.norm.rvs(size=N / 2, loc=0.2, scale=T)
        vp[N / 2:] = stats.norm.rvs(size=N / 2, loc=-0.2, scale=T)
    else:
        if vp == None:
            vp = stats.norm.rvs(size=N, loc=V0, scale=T)
        else:
            vp = vp  # vp is already specified

    number_save_samples = NT / NTOUT
    if save_last:
        number_save_samples += 1

    # Create arrays to put the data in that needs to be saved
    save_xp = np.zeros((number_save_samples, N))
    save_vp = np.zeros((number_save_samples, N))
    # save initial positions and velocities
    save_xp[0] = xp
    save_vp[0] = vp
    if save_all:
        # These quantities are unknown now, so at t=0 they are zero
        save_E = np.zeros((number_save_samples, NG))
        save_E_particle = np.zeros((number_save_samples, N))
        save_GC = np.zeros((number_save_samples, NG))
        save_phi = np.zeros((number_save_samples, NG))
        save_potential = np.zeros((number_save_samples, N))

    # Run Simulation
    for t in range(1, NT):
        # Move particles
        xp = (xp + vp * DT) % L  # % L for periodicity

        # indices of the grid point each particle is in
        index = (xp / grid_size).astype(int)  # always round down, so index of left (lower) grid point

        # The fraction of the charge that will be placed at a grid point
        fraction = (xp - grid_positions[index]) / grid_size

        # move charge to grid points
        grid_charges = np.zeros(NG)
        for i in range(NG):
            grid_charges[(i + 1) % NG] -= sum(fraction[i == index])
            grid_charges[i] -= sum(1 - fraction[i == index])

        # Normalize the charge and add the ion charge, divide by grid_size to get charge density
        grid_charges = (grid_charges / N + 1. / NG) / grid_size

        # Solve the linear set of equations of the poisson equation
        phi = inverse_poisson_matrix.dot(-grid_charges * grid_size**2)
        # This does the same
        # phi = np.linalg.solve(poisson_matrix, -grid_charges * grid_size**2)


        # Simple phi check, second derivative is density again.
        # print np.diff(np.diff(phi)) / grid_size**2
        # print grid_charges

        # Calculate the electric field on grid points by
        # taking the derivative of phi using center point
        E = (np.roll(phi, 1) - np.roll(phi, -1)) / (2 * grid_size)

        # Calculate the electrical field on each particle through linear interpolation
        E_particle = fraction * E[(index + 1) % NG] + (1 - fraction) * E[index]

        # Update velocities of the particles
        vp = vp + E_particle * DT * QM

        if t % NTOUT == 0:
            save_vp[t / NTOUT] = vp
            save_xp[t / NTOUT] = xp

            if save_all:
                save_E[t / NTOUT] = E
                save_E_particle[t / NTOUT] = E_particle
                save_GC[t / NTOUT] = grid_charges
                save_phi[t / NTOUT] = phi
                save_potential[t / NTOUT] = (fraction * phi[(index + 1) % NG] + (1 - fraction) * phi[index]) * QM

    # Timer result
    print "It took %.3g seconds" % (time.time() - start)

    if save_all:
        if save_last:  # Save last point additionally
            save_xp[-1] = xp
            save_vp[-1] = vp
            save_E[-1] = E
            save_E_particle[-1] = E_particle
            save_GC[-1] = grid_charges
            save_phi[-1] = phi
            save_potential[-1] = (fraction * phi[(index + 1) % NG] + (1 - fraction) * phi[index]) * QM
        return save_xp, save_vp, save_E, save_E_particle, save_GC, save_phi, save_potential

    if animate:
        make_animation(save_xp, save_vp, N=N, NT=NT, NTOUT=NTOUT, **kwargs)

    else:
        if save_last:  # save last point if specified
            save_xp[-1] = xp
            save_vp[-1] = vp
        return save_xp, save_vp


def write_to_file(data, filenames):
    """
    Saves data to files of which the names are specified, 
    takes arrays of data blocks, as long as filenames is an array of the same length 
    """
    if type(filenames) != str:
        for i in range(len(filenames)):
            np.save(filenames[i], data[i])
    else:
        np.savetxt(data, filenames)


def make_animation(save_xp, save_vp, show=True, save=False, NT=500,
              NTOUT=1, N=1000, filename="PIC_sim.mp4"):
    """
    Animates the phase space in two colors, first half of the particles 
    are shown in red, second half in blue.
    Show: Shows the animation 
    save: Saves the animation before showing
    filename: The filename of the saved animation    
    """
    fig = plt.figure()
    left = plt.plot(save_xp[0, :N / 2], save_vp[0, :N / 2], "o", c="red")
    right = plt.plot(save_xp[0, N / 2:], save_vp[0, N / 2:], "o", c="blue")
    axes = plt.gca()
    axes.set_ylim([-1, 1])
    axes.set_xlabel("x")
    axes.set_ylabel("velocity")


    ani = animation.FuncAnimation(fig,
                                  update,
                                  frames=NT / NTOUT,
                                  fargs=(save_xp, save_vp, left, right, N),
                                  interval=20)
    if save:
        ani.save(filename, writer='ffmpeg', bitrate=-1, codec='h264')
    if show:
        plt.show()


def update(i, save_xp, save_vp, left, right, N):
    # Updates the animation, let and right specified for two streams and looking fancy
    left[0].set_xdata(save_xp[i, :N / 2])
    left[0].set_ydata(save_vp[i, :N / 2])
    right[0].set_xdata(save_xp[i, N / 2:])
    right[0].set_ydata(save_vp[i, N / 2:])
    return left, right


def numerical_heating(T=0.01, NT=10000, NTOUT=10000, DT=.5):
    """
    Tests the numerical heating
    """
    save_xp_100, save_vp_100 = PIC_simulation(N=100, T=T, NT=NT, NTOUT=NTOUT, DT=DT, save_last=True)
    save_xp_1000, save_vp_1000 = PIC_simulation(N=1000, T=T, NT=NT, NTOUT=NTOUT, DT=DT, save_last=True)
    save_xp_10000, save_vp_10000 = PIC_simulation(N=10000, T=T, NT=NT, NTOUT=NTOUT, DT=DT, save_last=True)

    print ("100 particles, standard deviation before: %.3g, after: %.3g" %
           (np.std(save_vp_100[0]), np.std(save_vp_100[-1])))
    print ("1000 particles, standard deviation before: %.3g, after: %.3g" %
           (np.std(save_vp_1000[0]), np.std(save_vp_1000[-1])))
    print ("10000 particles, standard deviation before: %.3g, after: %.3g" %
           (np.std(save_vp_10000[0]), np.std(save_vp_10000[-1])))

    bins = np.linspace(-0.05, 0.05, 30)
    f, ax_arr = plt.subplots(2, 3, figsize=(10, 5))
    ax_arr[0][0].hist(save_vp_100[0], bins=bins, normed=True, histtype="step", color="g", linestyle="dashed")
    ax_arr[0][0].hist(save_vp_100[-1], bins=bins, normed=True, histtype="step", color="r", lw=2)
    ax_arr[0][0].set_title("N=100")
    ax_arr[0][0].set_ylim([0, 50])
    ax_arr[0][1].hist(save_vp_1000[0], bins=bins, normed=True, histtype="step", color="g", linestyle="dashed")
    ax_arr[0][1].hist(save_vp_1000[-1], bins=bins, normed=True, histtype="step", color="r", lw=2)
    ax_arr[0][1].set_title("N=1000")
    ax_arr[0][1].set_ylim([0, 50])
    ax_arr[0][1].tick_params(labelleft="off")
    ax_arr[0][2].hist(save_vp_10000[0], bins=bins, normed=True, histtype="step", color="g", linestyle="dashed")
    ax_arr[0][2].hist(save_vp_10000[-1], bins=bins, normed=True, histtype="step", color="r", lw=2)
    ax_arr[0][2].set_title("N=10000")
    ax_arr[0][2].set_ylim([0, 50])
    ax_arr[0][2].tick_params(labelleft="off")

    ax_arr[0][0].set_ylabel("Normalized counts per bin")
    ax_arr[0][1].set_xlabel("Velocity")

    times = DT * NTOUT * np.arange(NT / NTOUT + 1)
    ax_arr[1][0].plot(times, np.std(save_vp_100, ddof=1, axis=1))
    ax_arr[1][0].set_ylim([0.009, 0.05])
    ax_arr[1][0].set_yscale("log")
    ax_arr[1][0].yaxis.set_minor_formatter(FormatStrFormatter("%.2f"))
    ax_arr[1][0].tick_params(labelleft="off", which='major')
    ax_arr[1][1].plot(times, np.std(save_vp_1000, ddof=1, axis=1))
    ax_arr[1][1].set_ylim([0.009, 0.05])
    ax_arr[1][1].set_yscale("log")
    ax_arr[1][1].tick_params(labelleft="off", which='both')
    ax_arr[1][2].plot(times, np.std(save_vp_10000, ddof=1, axis=1))
    ax_arr[1][2].set_ylim([0.009, 0.05])
    ax_arr[1][2].set_yscale("log")
    ax_arr[1][2].tick_params(labelleft="off", which='both')

    ax_arr[1][1].set_xlabel("Time")
    ax_arr[1][0].set_ylabel("VT")

    plt.tight_layout()
    f.subplots_adjust(wspace=0)
    plt.show()


def numerical_heating2(low_N=100, high_N=20000, samples=20):
    """
    Checks the numerical heating as function of number of particles and plots the velocity distributions
    At t=0 and t=5000 with time steps of 0.5 using 32 grid points and an initial thermal velocity of 0.01. 
    Kinda only works properly if samples % 4 = 0.
    """
    N_vals = np.logspace(np.log10(low_N), np.log10(high_N), samples).astype(int)

    f, ax_arr = plt.subplots(int(np.ceil(samples / 4.)), 4, sharey=True, sharex=True, figsize=(10, 10))
    bins = np.linspace(-0.05, 0.05, 30)
    initial_std = np.zeros(samples)
    final_std = np.zeros(samples)

    for i in range(samples):
        save_xp, save_vp = PIC_simulation(N=N_vals[i], T=0.01, NT=10000, NTOUT=10000, DT=.5, NG=32, save_last=True)

        ax_arr[i / 4][i % 4].hist(save_vp[0], bins=bins, normed=True, histtype="step", color="g", linestyle="dashed")
        ax_arr[i / 4][i % 4].hist(save_vp[-1], bins=bins, normed=True, histtype="step", color="r", lw=2)
        ax_arr[i / 4][i % 4].text(0.015, 40, "N=%i" % N_vals[i])
        ax_arr[i / 4][i % 4].set_ylim([0, 49])
        ax_arr[i / 4][i % 4].set_xlim([-0.049, 0.049])
        initial_std[i] = np.std(save_vp[0], ddof=1)
        final_std[i] = np.std(save_vp[-1], ddof=1)
        print ("%i particles, standard deviation before: %.3g, after: %.3g" %
               (N_vals[i], initial_std[i], final_std[i]))

    f.subplots_adjust(hspace=0, wspace=0)
    ax_arr[2][0].set_ylabel("Normalized counts per bin")
    ax_arr[4][0].set_xlabel("Velocity")
    ax_arr[4][1].set_xlabel("Velocity")
    ax_arr[4][2].set_xlabel("Velocity")
    ax_arr[4][3].set_xlabel("Velocity")

    plt.show()

    # Function to fit with
    scaling_function = lambda N, a, b: a * N**b
    # curve_fit the data to find a N dependence
    (a, b), spam = curve_fit(scaling_function, N_vals, final_std - initial_std, p0=(1., -0.5))

    print "The thermal broadening scales with N^%.3g" % b
    plt.scatter(N_vals, final_std - initial_std)
    plt.plot(np.linspace(50, 25000, 1000), scaling_function(np.linspace(50, 25000, 1000), a, b))
    plt.xscale("log")
    plt.yscale("log")
    plt.ylabel("$\Delta$ Thermal Velocity")
    plt.xlabel("Number of particles")
    plt.show()


def update_hist(i, x, v, ax):
    """
    Updates the 2d histogram, the commented part can be removed to use gaussian interpolation, 
    the plt.hist2d line must be removed then. Note, gaussian interpolation is quite slow. 
    """
    # H, xedges, yedges = np.histogram2d(x[i], v[i], bins=[50, 50], range=[[0, np.pi * 2], [-0.5, 0.5]])
    # ax = plt.imshow(np.rot90(H), interpolation="gaussian", aspect="auto")
    # ax.axes.get_xaxis().set_visible(False)
    # ax.axes.get_yaxis().set_visible(False)
    ax = plt.hist2d(x[i], v[i], bins=(xedges, yedges))
    # Print i to keep track of progress, as it can be quite slow
    print "Frame: ", i
    return ax


def hist2d_animation():
    """
    Makes an animated 2d histogram, note: it is written quite ugly
    """
    x, v = PIC_simulation(two_beam=True, animate=False, N=250000, NT=750, QM=-1, DT=0.5,
                          NTOUT=1, NG=128, T=0.02)
    f = plt.figure()
    # H, xedges, yedges = np.histogram2d(x[0], v[0], bins=[50, 50], range=[[0, np.pi * 2], [-0.5, 0.5]])
    # ax = plt.imshow(np.rot90(H), interpolation="gaussian", aspect="auto")
    # ax.axes.get_xaxis().set_visible(False)
    # ax.axes.get_yaxis().set_visible(False)

    # The edges of the bins of the histogram.
    global xedges, yedges
    xedges = np.linspace(0, np.pi * 2, 100)
    yedges = np.linspace(-0.5, 0.5, 100)
    ax = plt.hist2d(x[0], v[0], bins=(xedges, yedges))


    hist_ani = animation.FuncAnimation(f,
                                       update_hist,
                                       frames=750,
                                       fargs=(x, v, ax),
                                       interval=20,
                                       repeat=False)

    # Save histogram animation
    hist_ani.save("Hist2d.mp4", writer='ffmpeg') #, bitrate=-1, codec='h264')
    plt.show()


def test_conservation():
    """
    Tests various conservation laws by plotting them for inspection
    """
    # Need to fix the initial velocities for all cases
    v0 = stats.norm.rvs(size=5000, loc=0.2, scale=0.01)

    x1, v1, E1, Ep1, GC1, phi1, pot1 = PIC_simulation(save_all=True, NG=32, N=5000, vp=v0, DT=2.0, NT=125)
    x2, v2, E2, Ep2, GC2, phi2, pot2 = PIC_simulation(save_all=True, NG=32, N=5000, vp=v0, DT=0.1, NT=2500, NTOUT=20)
    x3, v3, E3, Ep3, GC3, phi3, pot3 = PIC_simulation(save_all=True, NG=128, N=5000, vp=v0, DT=2.0, NT=125)
    x4, v4, E4, Ep4, GC4, phi2, pot4 = PIC_simulation(save_all=True, NG=128, N=5000, vp=v0, DT=0.1, NT=2500, NTOUT=20)

    time = np.linspace(0, 250, 125)

    plt.plot(time, np.sum(v1**2, axis=1), label="NG=32, DT=2.0")
    plt.plot(time, np.sum(v2**2, axis=1), label="NG=32, DT=0.1")
    plt.plot(time, np.sum(v3**2, axis=1), label="NG=128, DT=2.0")
    plt.plot(time, np.sum(v4**2, axis=1), label="NG=128, DT=0.1")
    plt.xlabel("Time")
    plt.ylabel("$\sum_i v_i^2$")
    plt.legend()
    plt.tight_layout()
    plt.show()

    plt.plot(time, np.sum(v1, axis=1), label="NG=32, DT=2.0")
    plt.plot(time, np.sum(v2, axis=1), label="NG=32, DT=0.1")
    plt.plot(time, np.sum(v3, axis=1), label="NG=128, DT=2.0")
    plt.plot(time, np.sum(v4, axis=1), label="NG=128, DT=0.1")
    plt.xlabel("Time")
    plt.ylabel("$\sum_i v_i$")
    plt.legend()
    plt.tight_layout()
    plt.show()

    plt.plot(time, np.sum(Ep1**2, axis=1), label="NG=32, DT=2.0")
    plt.plot(time, np.sum(Ep2**2, axis=1), label="NG=32, DT=0.1")
    plt.plot(time, np.sum(Ep3**2, axis=1), label="NG=128, DT=2.0")
    plt.plot(time, np.sum(Ep4**2, axis=1), label="NG=128, DT=0.1")
    plt.xlabel("Time")
    plt.ylabel("$\sum_i |E_i|^2$")
    plt.legend()
    plt.tight_layout()
    plt.show()

    plt.plot(time, np.sum(pot1 - 0.5 * v1**2, axis=1), label="NG=32, DT=2.0")
    plt.plot(time, np.sum(pot2 - 0.5 * v2**2, axis=1), label="NG=32, DT=0.1")
    plt.plot(time, np.sum(pot3 - 0.5 * v3**2, axis=1), label="NG=128, DT=2.0")
    plt.plot(time, np.sum(pot4 - 0.5 * v4**2, axis=1), label="NG=128, DT=0.1")
    plt.xlabel("Time")
    plt.ylabel("Total energy")
    plt.legend()
    plt.tight_layout()
    plt.show()


def two_stream_plot(T=0):
    """
    Plots the phase space of the two stream instability at various times.  
    """
    x, v = PIC_simulation(two_beam=True, NT=750, T=T, NTOUT=50, N=100000)

    xedges = np.linspace(0, np.pi * 2, 100)
    yedges = np.linspace(-0.5, 0.5, 100)

    f, axarr = plt.subplots(5,3, sharex=True, sharey=True, figsize=(10,15))
    for i in range(15):
        axarr[i / 3][i % 3].hist2d(x[i], v[i], bins=[xedges, yedges])
        axarr[i / 3][i % 3].text(5, 0.4, "t=%i" % (i * 25), color="white")

    f.subplots_adjust(hspace=0, wspace=0)
    axarr[2][0].set_ylabel("Velocity")
    axarr[4][1].set_xlabel("x-coordinate")
    plt.show()


# The test for Matthew
x, v = PIC_simulation(N=10000, NT=1000, NG=64, NTOUT=100)

