from __future__ import division
import numpy as np
from scipy.sparse import spdiags
from scipy.sparse import csr_matrix
import matplotlib.pyplot as plt
import matplotlib.colors as mcl
import matplotlib.animation as animation
import matplotlib.cm as cm

import time

start_time = time.time()

np.set_printoptions(linewidth=3500)
np.set_printoptions(precision=3)

# Define some quantities
L=2.0 * np.pi # box length
dt=0.5 # timestep
NT=100 # total number of timesteps
NTOUT=25 # save output every other NTOUT
NG=64 # number of gridpoints DECIDE: is this the number of cells or number of edges?
N=10000 # number of particles
WP=1  
QM= -1 # charge over mass (normalized). For electrons = -1
V0=0.2  # mean initial velocity (one for each species)
VT=0.05 # thermal velocity (standard deviation for a Maxwellian distribution)
XP1=0.001 # initial perturbation
V1 = 0.0

Q=WP**2/(QM*N/L);
rho_back=-Q*N/L;
dx=L/NG;

un = np.ones(NG-1)

Poisson = spdiags([un,-2*un,un],[-1,0,1], NG-1,NG-1).toarray()
# Initialize particles
# position-> uniformly distributed in the box

#xp = np.random.uniform(0,L,N)
xp = np.linspace(0,L-L/N,num=N)
# velocity -> Normal distribution with std VT and mean V0
#V0 = 0
vp1 = VT*np.random.randn(1,int(N/2))+V0
vp2 = VT*np.random.randn(1,int(N/2))-V0
vp1 = np.array(vp1)
vp2 = np.array(vp2)
#vp = np.append(vp,vp2)

vp = np.empty((vp1.size + vp2.size,), dtype=vp1.dtype)
vp[0::2] = vp1
vp[1::2] = vp2

# Add a perturbation in the position (small displacement)
mode =1
xp = xp + XP1 * np.sin(2 * np.pi * xp / L * mode)
vp = vp+V1*np.sin(2*np.pi*xp/L*mode);
# check for periodic boundary conditions
p = np.linspace(0,N-1, N)
p = np.append(p,p,axis=0)
vp_begin = vp

#Adding color to the two streams
color = np.zeros(N,dtype=str)
color[np.where(vp < 0)] = 'r'
color[np.where(vp >= 0)] = 'b'
grid = np.linspace(0,NG,NG)

for it in np.linspace(1,NT,NT):
  
    xp =  xp + dt*vp

    xp[np.where(xp<0)] = xp[np.where(xp<0)] + L

    xp[np.where(xp >= L)] = xp[np.where(xp >= L)] - L
    # Accumulate density from Particles to Grid	(mind the BC)
    # and calculate charge density
    g1 = np.floor(xp/dx-.5)+1
    g2 = g1+1

    g = np.append(g1,g2,axis=0)

    fra1 = 1-np.abs(xp/dx-g1+0.5)

    fra = np.append(fra1, (1-fra1),axis = 0)
 
    g[np.where(g<0)] = g[np.where(g<0)] + NG
    g[np.where(g >= NG)] = g[np.where(g >= NG)] - NG

    mat = csr_matrix((fra,(p,g)),shape=(N,NG)).toarray()
    
    rho = (Q/dx)*np.sum(mat,axis=0)+rho_back

    if( rho[0] == rho[NG-1]):
        print TRUE

    Phi = np.linalg.solve(Poisson,-rho[0:NG-1]*dx**2)
    Phi = np.append(Phi,[0],axis=0)
    Eg1  = np.append([Phi[NG-1]],Phi[0:NG-1],axis=0)
    Eg2 = np.append(Phi[1:NG+1],[Phi[0]])

    Eg = (Eg1-Eg2)/(2*dx)
    
    # Update particle velocity with leapfrog
    vp = vp + np.dot(mat,Eg)*QM*dt

print (time.time()-start_time) 
plt.scatter(xp,vp,color=color)
plt.show()