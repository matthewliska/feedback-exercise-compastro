
"""
PIC Exercise, Emma van der Wateren
"""

import numpy as np
import matplotlib.pyplot as plt


L = 2.0 * np.pi # box length
DT = 0.5 # timestep
NT = 500 # total number of timesteps
NG = 32. # number of gridpoints
lgrid = L/NG # size of the cells
N = 10000 # number of particles
QM = -1 # charge over mass (normalized). For electrons = -1
V0 = 0.2  # mean initial velocity (one for each species)
VT = 0.01 # Initial thermal velocity
DX = L/NG # Step size in x
XP1 = 0.001 # initial perturbation

# Some vectors
grid = np.arange(NG)*(L/NG)
rho = np.zeros(NG)
phi = np.zeros((NG+1,NG+1))
E = np.zeros(NG)
parts = np.zeros(NG)
Ep = np.zeros(N)
VTs = np.zeros(NT)
VTs[0] = VT
Ekin = np.zeros(NT)
Ee = np.zeros(NT)
P = np.zeros(NT)
fig = plt.figure(figsize=(10,10)) # Set up our figure - we will update it as we go
ax = fig.add_subplot(1,1,1)
ax2 = fig.add_subplot(1,1,1)

# Initialize particles
def particles(N,L):

    # position-> uniformly distributed in the box
    xp = np.arange(0,N)*(L/N)

    # velocity -> Normal distribution with std VT and mean V0
    vp = np.random.normal(loc = V0, scale = VT, size = N)

    # Add a perturbation in the position (small displacement)
    mode = 1
    xp = xp + XP1 * np.sin(2 * np.pi * xp / L * mode)

    # check for periodic boundary conditions
    for k in range(len(xp)):
        if xp[k] < 0:
            xp[k] = L - xp[k]
        if xp[k] > L:
            xp[k] = xp[k] % L
    
    return xp, vp

def Poisson_matrix(grid, DX):
    for j in range(len(grid)):
        phi[j][j - 1] = 1./DX**2
        phi[j][j] = -2/DX**2
        if j + 1 == NG:
            a = 0
            phi[j][a] = 1/DX**2
        else:
            a = j + 1
            phi[j][a] = 1/DX**2
    return phi, a

xp = particles(N,L)[0]
vp = particles(N,L)[1]
phi = Poisson_matrix(grid, DX)[0]
a = Poisson_matrix(grid, DX)[1]

# Main loop
for i in range(NT):
    # Mover
    
    # Thermal velovity
    VTs[i] = VT
    
    # Kinetic energy
    Ekin[i] = np.sum(vp**2)
    
    # Linear momentum
    P[i] = np.sum(vp)
    
    xp = xp + vp * DT
    
    # Check for periodic boundary conditions
    for k in range(len(xp)):
        if xp[k] < 0:
            xp[k] = L - xp[k]
        if xp[k] > L:
            xp[k] = xp[k] % L
    
    # Accumulate density from Particles to Grid
    for j in range(len(grid)):
        
        # First gather all the particles involved in the cell
        parts = xp[np.logical_and(xp > (grid[j] - lgrid), xp <= grid[j] + lgrid)]

        # Than calculate the charge density in that cell
        rho[j] = (np.sum(np.abs((np.abs(parts-grid[j]))/4.-1.)))/N

        # Solve Poisson's equation with the new charge density	
        phis = np.linalg.solve(phi[0:NG,0:NG],rho)
        
        # and then E = - grad(phi)
        E[j] = - (phis[a] - phis[j-1]) / (L/NG)**2 
    
    # Electric energy
    Ee[i] = np.sum(E**2)
    
    # Calculate Electric field on the position of the particles	
    for h in range(len(xp)):
        
        # First find the fields involved for every particle
        fields = E[np.logical_and(grid > (xp[h] - lgrid), grid < (xp[h] + lgrid))]
        
        fpart = np.zeros(len(fields))
        
        # Than calculate the field weighted for the particles
        for l in range(len(fields)):
            fpart[l] =  fields[l]*np.abs((np.abs(xp[h]-grid[np.where(E == fields[l])]))/4.-1.)
        
        Ep[h] = np.sum(fpart)
        
        # Taking care of the boundaries
        if xp[h] > (L - lgrid):
            fields = E[0]
            fpart = fields * xp[h] - (L - lgrid)/4.
            Ep[h] = Ep[h] + fpart
        if xp[h] < (lgrid):
            fields = E[-1]
            fpart = fields * (lgrid - xp[h])/4.
            Ep[h] = Ep[h] + fpart
                

    # Update particle velocity
    vp=vp + QM * Ep * DT
    
    # Find new thermal velocity
    VT = np.std(vp)

    x = np.arange(0,NG)*(L/NG)
    t = np.arange(0,NT)

    plt.figure(1)
    if i == 0:
        plt.plot(xp,vp,linewidth=2,color='red')
        plt.xlabel('Position', fontsize = 15)
        plt.ylabel('Momentum', fontsize = 15)
        plt.title('Phase space', fontsize = 20)
    if (i % 20 == 0):    
        plt.plot(xp,vp,linewidth=2,color='red')
    plt.figure(2)
    if i == 0:
        plt.plot(x,E,linewidth=2,color='red')
        plt.xlabel('Position', fontsize = 15)
        plt.ylabel('Electric field', fontsize = 15)
        plt.title('The electric field in the rod', fontsize = 20)
    if (i % 20 == 0):    
        plt.plot(x,E,linewidth=2,color='red')
    plt.figure(3)
    if i == 0:
        n, bins, patches = plt.hist(vp, 50, color = 'red')
        plt.xlabel('Thermal velocity', fontsize = 15)
        plt.title('Distribution of thermal velocities', fontsize = 20)

plt.figure(4)
plt.plot(t,VTs)
plt.xlabel('Time', fontsize = 15)
plt.ylabel('Thermal velocity', fontsize = 15)
plt.title('Evolution of thermal velocity', fontsize = 20)

plt.figure(5)
plt.plot(t,Ekin)
plt.xlabel('Time', fontsize = 15)
plt.ylabel('Kinetic energy', fontsize = 15)
plt.title('Evolution of kinetic energy', fontsize = 20)

plt.figure(6)
plt.plot(t,Ee)
plt.xlabel('Time', fontsize = 15)
plt.ylabel('Electric energy', fontsize = 15)
plt.title('Evolution of electric energy', fontsize = 20)

plt.figure(7)
plt.plot(t,Ee+Ekin)
plt.xlabel('Time', fontsize = 15)
plt.ylabel('Total energy', fontsize = 15)
plt.title('Evolution of total energy', fontsize = 20)

plt.figure(8)
plt.plot(t,P)
plt.xlabel('Time', fontsize = 15)
plt.ylabel('Linear momentum', fontsize = 15)
plt.title('Evolution of linear momentum', fontsize = 20)

plt.show()


