# Define some quantities
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

L=2.0 * np.pi # box length
DT=0.001 # timestep
NT=500 # total number of timesteps
NTOUT=1 # save output every other NTOUT
NG=32.0 # number of gridpoints DECIDE: is this the number of cells or number of edges?
N=1000 # number of particles
WP=1  
QM=- 1 # charge over mass (normalized). For electrons = -1
V0=0.2  # mean initial velocity (one for each species)
T=0.0  # thermal velocity (standard deviation for a Maxwellian distribution)
XP1=0.001 # initial perturbation
DELTA = (1/NG)*L
# Some more?

# Initialize particles
# position-> uniformly distributed in the box
xlocs = np.random.uniform(0,L,N)
x_grid = np.linspace(0,L,NG,endpoint=False)


# velocity -> Normal distribution with std VT and mean V0
v_x = np.random.normal(0.2,0.01,N)


# Add a perturbation in the position (small displacement)
mode=1
xlocs=xlocs + XP1 * np.sin(2 * np.pi * xlocs / L * mode)
# check for periodic boundary conditions

#Matrix
matrix = []
for k in range(len(x_grid)):
    matrix_line = np.zeros(32)
    if k == (len(x_grid)-1):
        matrix_line[k-1] = 1
        matrix_line[k] = -2
        matrix_line[0] = 0 #BC
        matrix.append(matrix_line)
    if k != 0 and k!= (len(x_grid)-1):
        matrix_line[k-1] = 1
        matrix_line[k] = -2
        matrix_line[k+1] = 1
        matrix.append(matrix_line)
    if k == 0:
        matrix_line[len(x_grid)-1] = 0 #BC
        matrix_line[k] = -2
        matrix_line[k+1] = 1
        matrix.append(matrix_line)

        
# Main Loop
xlocs_list_anim = []
v_xlist_anim = []
for i in range(NT):
    E_field = []
    # Mover --> Use leapfrog to update positions
    xlocs = (xlocs + DT*v_x) % L
    xlocs_list_anim.append(xlocs)
    # check for periodic boundary conditions

    # Accumulate density from Particles to Grid	(mind the BC)
    # and calculate charge density
    accum_rho_list = []
    for j in range(len(x_grid)):
        if j == len(x_grid)-1:
            accum_rho_per_grid_point_1 = sum(QM*WP*(xlocs[(x_grid[0] >= (xlocs-L)) & (xlocs >= x_grid[j]) ] - ((x_grid[j])-L))/DELTA)
            accum_rho_per_grid_point_2 = sum(QM*WP*(1-(xlocs[(x_grid[0] >= (xlocs-L)) & (xlocs >= x_grid[j]) ] - ((x_grid[j])-L))/DELTA))
            accum_rho_list.append(((accum_rho_per_grid_point_1+accum_rho_per_grid_point_2)/N)+(1/NG))
            charge_array = np.array(accum_rho_list)
        elif j != len(x_grid)-1:
            accum_rho_per_grid_point_1 = sum(QM*WP*(xlocs[(x_grid[j+1] >= xlocs) & (xlocs >= x_grid[j]) ] - (x_grid[j]))/DELTA)
            accum_rho_per_grid_point_2 = sum(QM*WP*(1-(xlocs[(x_grid[j+1] >= xlocs) & (xlocs >= x_grid[j]) ] - (x_grid[j]))/DELTA))
            accum_rho_list.append(((accum_rho_per_grid_point_1+accum_rho_per_grid_point_2)/N)+(1/NG))
            charge_array = np.array(accum_rho_list)     
    # Solve Poisson's equation with the new charge density	
    # (solve first laplacian(phi) = -rho
    # and then E = - grad(phi)
    phi = np.linalg.solve(matrix,charge_array)
    E_field_tot = []
    for k in range(len(phi)):
        if k == len(phi)-1:
            E_field = -1*(phi[0] - phi[k-1])/(2*DELTA)
            E_field_tot.append(E_field)
        elif k == 0:
            E_field = -1*(phi[k+1] - phi[len(phi)-1])/(2*DELTA)
            E_field_tot.append(E_field)
        elif k != 0:
            E_field = -1*(phi[k+1] - phi[k-1])/(2*DELTA)
            E_field_tot.append(E_field)
            
    E_field_tot = np.array(E_field_tot)

    
    meancheck = sum(E_field_tot)/len(E_field_tot)
    # Once again: are your boundary conditions satisfied?
    # Quick test: is mean(E)=0	satisfied?

    # Calculate Electric field on the position of the particles
    # (interpolate particle --> grid)
    E_per_particle_list = []
    for l in range(N):
        x = int(xlocs[l]/DELTA)
        if x == len(x_grid)-1:
            E_per_particle_right = E_field_tot[x]*(xlocs[l]-x_grid[x])/DELTA
            E_per_particle_left = E_field_tot[0]*(x_grid[0]-(xlocs[l]-L))/DELTA
            E_per_particle = E_per_particle_right + E_per_particle_left
            E_per_particle_list.append(E_per_particle)
        elif x != len(x_grid)-1:
            E_per_particle_right = E_field_tot[x]*(xlocs[l]-x_grid[x])/DELTA
            E_per_particle_left = E_field_tot[x+1]*(x_grid[x+1]-xlocs[l])/DELTA
            E_per_particle = E_per_particle_right + E_per_particle_left
            E_per_particle_list.append(E_per_particle)

    E_per_particle_array = np.array(E_per_particle_list)

    # Update particle velocity with leapfrog
    v_x=v_x + QM * E_per_particle_array * DT
    v_xlist_anim.append(v_x)
    # Put your diagnostics here: calculate energy and momentum 
    
    # Make plots, movies, etc.
v_xlist_anim = np.array(v_xlist_anim)
xlocs_list_anim = np.array(xlocs_list_anim) 

def make_animation(x_anim, v_anim, filename="PIC_sim.mp4"):

    fig = plt.figure()
    left = plt.plot(x_anim[0, :N / 2], v_anim[0, :N / 2], "o", c="red")
    right = plt.plot(x_anim[0, N / 2:], v_anim[0, N / 2:], "o", c="blue")
    axes = plt.gca()
    axes.set_ylim([-0.5, 0.5])
    axes.set_xlabel("Length")
    axes.set_ylabel("Velocity")


    ani = animation.FuncAnimation(fig,
                                  anim_update,
                                  frames= NT,
                                  fargs=(x_anim, v_anim, left, right, N))
    if False:
        ani.save(filename, writer='ffmpeg', bitrate=-1, codec='h264')
    if True:
        plt.show()


def anim_update(i, x_anim, v_anim, left, right, N):
    left[0].set_xdata(x_anim[i, :N / 2])
    left[0].set_ydata(v_anim[i, :N / 2])
    right[0].set_xdata(x_anim[i, N / 2:])
    right[0].set_ydata(v_anim[i, N / 2:])
    return left, right

print make_animation(xlocs_list_anim, v_xlist_anim)

