import numpy as np
import matplotlib.pyplot as plt

mode = 2 # Could be 1 for exercise 1 or 2 for exercises 2 and 3
interactive = False  # Setting interactive to True will show interactive plots every 500 time-steps.
# Setting it to false will just show the final result after 10000 time-steps

# Simulation parameters:
box_length = 2. * np.pi
num_nodes = 65
num_cells = num_nodes - 1
cell_size = box_length / (num_nodes - 1)

if mode == 1:  # For exercise 1 we don't need interactive mode.
    interactive = False
    num_particles = 1000
    dt = .05
    cycles = 2000
else:  # Parameter for exercises 2 and 3
    # Heated beams simulation:
    # num_particles = 10000
    # dt = .01
    # cycles = 2500

    # Cold beams simulation:
    num_particles = 10000
    dt = .01
    cycles = 10000

tot_time = cycles * dt

# Constant values:
wp = 1.  # Plasma frequency
part_charge = -1.  # Charge of electrons
charge = np.power(wp, 2) / (part_charge * num_particles / box_length)  # Numerical charge
init_pert_x = .001
background = -1. * charge * num_particles / box_length  # Background charge given by ions
if mode == 1:
    thermal_velocity = .01
    v_init = 0.
else:
    thermal_velocity = 0.
    v_init = .2

# Initializing some arrays
rho = np.zeros(num_nodes)
field = np.zeros(num_nodes)

time = np.linspace(0., tot_time, cycles)
kinetic = np.zeros(cycles)
potential = np.zeros(cycles)
tot_energy = np.zeros(cycles)
momentum = np.zeros(cycles)

# Matrix for field calculation:
matrix = np.matrix(np.zeros([num_nodes, num_nodes]))
for j in range(num_nodes):
    matrix[j, j] = -2.
    matrix[j - 1, j] = 1.
    matrix[j, j - 1] = 1.
matrix[0, -1] = 0.
matrix[-1, 0] = 0.


# Functions:
def charge_per_cell(pos, dx, nodes, q):
    """This function calculates the charge density in each cell
    given by the particles in them

    :param pos: Position of each particle in the box
    :param dx: Size of the cells in the grid
    :param nodes: Number of nodes in the grid
    :param q: Numerical charge of each particle
    :return: The total density of every cell
    """
    density = np.zeros(nodes)

    pos_in_cell = pos / dx
    index_pos = np.floor(pos_in_cell).astype('int')
    weight = pos_in_cell - index_pos
    for i in range(len(pos)):
        density[index_pos[i]] += q * (1. - weight[i]) / dx
        density[index_pos[i] + 1] += q * weight[i] / dx

    # Periodic boundary conditions
    density[0] += density[-1]
    density[-1] = density[0]

    total_density = density + background

    return total_density


def field_solver(e_field, density, dx, m):
    """This function calculates the electric field of
     each cell, given by the charge density.

    :param e_field: Electric field per cell
    :param density: Charge density
    :param dx: Size of the cells in the grid
    :param m: Matrix to solve field equation
    :return: Electric field per cell
    """
    dens = -density * np.power(dx, 2)
    phi = np.linalg.solve(m, dens)

    e_field[1:-1] = -(phi[2:] - phi[:-2]) / (2 * dx)
    e_field[0] = -(phi[1] - phi[-1]) / (2 * dx)
    e_field[-1] = -(phi[0] - phi[-2]) / (2 * dx)

    return e_field, phi


def particle_mover(x, v, e_field, dx):
    """This function "moves" the particle in the grid.
    Calculating the influence of the electric field of the cell
    in each particle that is in it.

    :param x: Position of each particle
    :param v: Velocity of each particle
    :param e_field: Electric field per cell
    :param dx: Size of the cells in the grid
    :return: New position and velocities
    """
    x += v * dt

    # Periodic boundary conditions:
    for i in range(len(x)):
        if x[i] >= box_length:
            x[i] -= box_length
        if x[i] < 0:
            x[i] += box_length

    pos_in_cell = x / dx
    index_pos = np.floor(pos_in_cell).astype('int')
    weight = pos_in_cell - index_pos
    for i in range(len(v)):
        v[i] += part_charge * ((1. - weight[i]) * e_field[index_pos[i]] + weight[i] * e_field[index_pos[i] + 1]) * dt

    return x, v

# initialization
positions = np.linspace(0, box_length, num_particles, endpoint=False)

if mode == 2:  # For exercise 3 we need to insert a perturbation in the position of the particles
    mode_pert = 1
    for k in range(len(positions)):
        positions[k] += init_pert_x * np.sin(2. * np.pi * positions[k] / box_length * mode_pert)
        # Periodic boundary conditions:
        if positions[k] >= box_length:
            positions[k] -= box_length
        if positions[k] < 0:
            positions[k] += box_length

velocities = np.zeros(len(positions))

velocities[range(0, num_particles - 1, 2)] = v_init
velocities[range(1, num_particles, 2)] = -1. * v_init

# Heated beams:
# velocities += np.random.normal(0., thermal_velocity, len(velocities))

if mode == 1:  # For exercise 1 velocities have a normal distribution around the thermal velocity
    velocities = np.random.normal(0., thermal_velocity, len(velocities))
    initial_velocities = np.random.normal(0., thermal_velocity, len(velocities))

# Plotting parameters:
fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(14, 10))

if interactive:
    plt.ion()

for step in range(cycles):
    rho = charge_per_cell(positions, cell_size, num_nodes, charge)
    field, phi = field_solver(field, rho, cell_size, matrix)

    positions, velocities = particle_mover(positions, velocities, field, cell_size)

    # Saving historic values:
    kinetic[step] = np.sum(np.power(velocities, 2))
    potential[step] = np.sum(np.power(field, 2))
    tot_energy[step] = kinetic[step] + potential[step]
    momentum[step] = np.sum(velocities)

    if mode == 2:
        if interactive:
            if step % 500 == 0:
                ax1.cla()
                ax2.cla()
                ax3.cla()

                ax1.set_title('Phase space for %i particles after %i time-steps' % (num_particles, step))
                ax1.set_xlabel('Particle position')
                ax1.set_ylabel('Particle velocity')
                ax1.set_xlim(0., box_length)
                ax1.set_ylim(-.5, .5)
                ax1.scatter(positions[range(0, num_particles - 1, 2)], velocities[range(0, num_particles - 1, 2)],
                            lw=0, color='blue')
                ax1.scatter(positions[range(1, num_particles, 2)], velocities[range(1, num_particles, 2)],
                            color='red')

                ax2.hexbin(positions, velocities, gridsize=64, bins='log', cmap='viridis')
                ax2.set_xlabel('Particle position')
                ax2.set_ylabel('Particle velocity')
                ax2.set_xlim(0., box_length)
                ax2.set_ylim(-.5, .5)

                ax3.hist(velocities, bins=100)
                ax3.set_xlabel(r'Velocity ($v_p$)')
                ax3.set_ylabel(r'Number of particles')

                plt.pause(0.0001)

if mode == 2:
    if not interactive:
        ax1.set_title('Phase space for %i particles after %i time-steps' % (num_particles, cycles))
        ax1.scatter(positions[range(0, num_particles - 1, 2)], velocities[range(0, num_particles - 1, 2)],
                    lw=0, color='blue')
        ax1.scatter(positions[range(1, num_particles, 2)], velocities[range(1, num_particles, 2)],
                    color='red')
        ax1.set_xlabel('Particle position')
        ax1.set_ylabel('Particle velocity')
        ax1.set_xlim(0., box_length)
        ax1.set_ylim(-.5, .5)

        ax2.hexbin(positions, velocities, gridsize=64, bins='log', cmap='viridis')
        ax2.set_xlabel('Particle position')
        ax2.set_ylabel('Particle velocity')
        ax2.set_xlim(0., box_length)
        ax2.set_ylim(-.5, .5)

        ax3.hist(velocities, bins=100)
        ax3.set_xlabel(r'Velocity ($v_p$)')
        ax3.set_ylabel(r'Number of particles')

        plt.savefig('final_state.pdf')

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 10))

    # Kinetic energy:
    ax1.set_title(r'Historic Kinetic Energy')
    ax1.set_xlabel('Time')
    ax1.set_ylabel(r'$W_k$')
    ax1.set_xlim(0, tot_time)
    ax1.plot(time, kinetic, lw=1.5, color='black')

    # Potential electric energy:
    ax2.set_title('Historic Potential Electric Energy')
    ax2.set_xlabel('Time')
    ax2.set_ylabel(r'$W_E$')
    ax2.set_xlim(0, tot_time)
    ax2.plot(time, potential, lw=1.5, color='blue')

    # Kinetic energy:
    ax3.set_title('Historic Total Energy')
    ax3.set_xlabel('Time')
    ax3.set_ylabel(r'$E$')
    ax3.set_xlim(0, tot_time)
    ax3.plot(time, tot_energy, lw=1.5, color='crimson')

    # Kinetic energy:
    ax4.set_title('Historic Linear Momentum')
    ax4.set_xlabel('Time')
    ax4.set_ylabel(r'$P$')
    ax4.set_xlim(0, tot_time)
    ax4.plot(time, momentum, lw=1.5, color='green')

    plt.savefig('energy_conservation.pdf')

if mode == 1:
    fig, ax = plt.subplots(1, figsize=(8, 8))
    ax.hist(velocities, bins=64, color='blue', alpha=.5, normed=True, label=('%i time-steps' % cycles))
    ax.hist(initial_velocities, bins=64, color='crimson', alpha=.5, normed=True, label='Initial')
    ax.set_title(r'Numerical heating for %i particles and $v_{th} = %.2f$' % (num_particles, thermal_velocity))
    ax.set_xlabel(r'Velocity ($v_p$)', fontsize=15)
    ax.set_ylabel(r'Number of particles', fontsize=15)

    ax.legend(loc=2)
    plt.savefig('numerical_heating.pdf')
