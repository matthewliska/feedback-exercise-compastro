import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

heated = True  # Setting it to true will show an animation with heated beams, false with cold beams

# Simulation parameters:
box_length = 2. * np.pi
num_nodes = 65
num_cells = num_nodes - 1
cell_size = box_length / (num_nodes - 1)

num_particles = 10000
dt = .05
cycles = 5000

tot_time = cycles * dt

# Constant values:
wp = 1.  # Plasma frequency
part_charge = -1.  # Charge of electrons
charge = np.power(wp, 2) / (part_charge * num_particles / box_length)  # Numerical charge
init_pert_x = .001
background = -1. * charge * num_particles / box_length  # Background charge given by ions
v_init = .2
if heated:
    thermal_velocity = .05
else:
    thermal_velocity = 0.

# Initializing some arrays
rho = np.zeros(num_nodes)
field = np.zeros(num_nodes)

anim_pos = np.matrix(np.zeros([cycles/100, num_particles]))
anim_vel = np.matrix(np.zeros([cycles/100, num_particles]))

# Matrix for field calculation:
matrix = np.matrix(np.zeros([num_nodes, num_nodes]))
for j in range(num_nodes):
    matrix[j, j] = -2.
    matrix[j - 1, j] = 1.
    matrix[j, j - 1] = 1.
matrix[0, -1] = 0.
matrix[-1, 0] = 0.


# Functions:
def charge_per_cell(pos, dx, nodes, q):
    """This function calculates the charge density in each cell
    given by the particles in them

    :param pos: Position of each particle in the box
    :param dx: Size of the cells in the grid
    :param nodes: Number of nodes in the grid
    :param q: Numerical charge of each particle
    :return: The total density of every cell
    """
    density = np.zeros(nodes)

    pos_in_cell = pos / dx
    index_pos = np.floor(pos_in_cell).astype('int')
    weight = pos_in_cell - index_pos
    for i in range(len(pos)):
        density[index_pos[i]] += q * (1. - weight[i]) / dx
        density[index_pos[i] + 1] += q * weight[i] / dx

    # Periodic boundary conditions
    density[0] += density[-1]
    density[-1] = density[0]

    total_density = density + background

    return total_density


def field_solver(e_field, density, dx, m):
    """This function calculates the electric field of
     each cell, given by the charge density.

    :param e_field: Electric field per cell
    :param density: Charge density
    :param dx: Size of the cells in the grid
    :param m: Matrix to solve field equation
    :return: Electric field per cell
    """
    dens = -density * np.power(dx, 2)
    phi = np.linalg.solve(m, dens)

    e_field[1:-1] = -(phi[2:] - phi[:-2]) / (2 * dx)
    e_field[0] = -(phi[1] - phi[-1]) / (2 * dx)
    e_field[-1] = -(phi[0] - phi[-2]) / (2 * dx)

    return e_field, phi


def particle_mover(x, v, e_field, dx):
    """This function "moves" the particle in the grid.
    Calculating the influence of the electric field of the cell
    in each particle that is in it.

    :param x: Position of each particle
    :param v: Velocity of each particle
    :param e_field: Electric field per cell
    :param dx: Size of the cells in the grid
    :return: New position and velocities
    """
    x += v * dt

    # Periodic boundary conditions:
    for i in range(len(x)):
        if x[i] >= box_length:
            x[i] -= box_length
        if x[i] < 0:
            x[i] += box_length

    pos_in_cell = x / dx
    index_pos = np.floor(pos_in_cell).astype('int')
    weight = pos_in_cell - index_pos
    for i in range(len(v)):
        v[i] += part_charge * ((1. - weight[i]) * e_field[index_pos[i]] + weight[i] * e_field[index_pos[i] + 1]) * dt

    return x, v

# initialization
positions = np.linspace(0, box_length, num_particles, endpoint=False)


mode_pert = 1
for k in range(len(positions)):
    positions[k] += init_pert_x * np.sin(2. * np.pi * positions[k] / box_length * mode_pert)
    # Periodic boundary conditions:
    if positions[k] >= box_length:
         positions[k] -= box_length
    if positions[k] < 0:
        positions[k] += box_length

velocities = np.zeros(len(positions))

velocities[range(0, num_particles - 1, 2)] = v_init
velocities[range(1, num_particles, 2)] = -1. * v_init

# Heated beams:
if heated:
    velocities += np.random.normal(0., thermal_velocity, len(velocities))


# Plotting setting for animation:
fig = plt.figure(figsize=(14, 6))
ax = fig.add_subplot(111, xlim=(0., box_length), ylim=(-.5, .5))
ax.set_title('Phase space for %i particles with $v_{th} = $%.2f' % (num_particles, thermal_velocity))
ax.set_xlabel('Particle position')
ax.set_ylabel('Particle velocity')
blue_points, = ax.plot([], [], color='blue', marker='o', linestyle='', lw=0)
red_points, = ax.plot([], [], color='red', marker='o', linestyle='', lw=0)


def init():
    blue_points.set_data([], [])
    red_points.set_data([], [])
    return blue_points, red_points,


def animate(i):
    global positions, velocities, field, charge
    for i in range(1):
        rho = charge_per_cell(positions, cell_size, num_nodes, charge)
        field, phi = field_solver(field, rho, cell_size, matrix)

        positions, velocities = particle_mover(positions, velocities, field, cell_size)

        blue_points.set_data(positions[range(0, num_particles - 1, 2)], velocities[range(0, num_particles - 1, 2)])
        red_points.set_data(positions[range(1, num_particles, 2)], velocities[range(1, num_particles, 2)])
    return blue_points, red_points,

anim = animation.FuncAnimation(fig, animate, frames=cycles, interval=100, blit=False, init_func=init)

#plt.show()
anim.save('two_stream_heated.mp4')